#!/bin/bash
url_escape() {
  echo `echo $1 | sed s:{:%7B:g | sed s:}:%7D:g`
}
URL=`url_escape "https://api.bitbucket.org/internal/repositories/$BITBUCKET_REPO_OWNER_UUID/$BITBUCKET_REPO_UUID/commit/$BITBUCKET_COMMIT/reports/1"`
curl -X DELETE -u synopsys-sig:$APP_PASSWORD $URL
curl -X PUT -H 'Content-Type: application/json' -u synopsys-sig:$APP_PASSWORD $URL -d '{"title": "BlackDuck by Synopsys SCA Report", "details": "Manage security, quality, and license compliance risk that comes from the use of open source and third-party code in applications and containers."}'